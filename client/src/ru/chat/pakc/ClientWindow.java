package ru.chat.pakc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener {

    private static final String IP_ADDRES = "192.168.0.16";    // задаем ip
    private static final int PORT = 8189;                      // задаем порт
    private static final int WIDTH = 600;                      // высота окна
    private static final int HEIGHT = 600;                     // ширина окна

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientWindow();
            }
        });

    }

    private final JTextArea log = new JTextArea();
    private final JTextField fieldNickname = new JTextField("pakc");
    private final JTextField fieldInput = new JTextField();

    private TCPConnection connection;

    private ClientWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);   // добовление закрытия окна (красный крестик в углу)
        setSize(WIDTH, HEIGHT);             // передаем параметры для отображения окна
        setLocationRelativeTo(null);        // задает что бы окно появлялось посередине
        setAlwaysOnTop(true);               // делает окно сверху всех окон и не уберается
        log.setEditable(false);             // запретить редактировать
        log.setLineWrap(true);              // автомотический перенос слов
        fieldInput.addActionListener(this); // позволяет перехватить событие нажатия "enter"

        add(fieldInput, BorderLayout.SOUTH); // добовление JFrame "fieldInput" на юг
        add(log, BorderLayout.CENTER);      //добовления JFrame "log" в центр
        add(fieldNickname, BorderLayout.NORTH); // добовление JFrame "fieldNickname" на север


        setVisible(true);                // делает окно видемым
        try {
            connection = new TCPConnection(this, IP_ADDRES, PORT);
        } catch (IOException e) {
            printMag("Connection exception: " + e);
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String msg = fieldInput.getText();
        if (msg.equals("")) return;
        fieldInput.setText(null);
        connection.sendString(fieldNickname.getText() + ": " + msg);
    }


    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMag("Connection ready ...");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMag(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMag("Connection close");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        printMag("Connection exception: " + e);
    }

    private synchronized void printMag(String mag) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(mag + "\n");
                log.setCaretPosition(log.getDocument().getLength());  //делает скрол  сто процентово
            }
        });
    }
}
